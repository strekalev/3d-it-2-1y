﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjManager : MonoBehaviour
{
    public GameObject prefab;
    static public int countObj = 0;
    public int countObjMax = 5;

    void Update()
    {
        if(countObj < countObjMax)
        {
            Instantiate(prefab, this.transform);
            countObj++;
        }

    }
}
