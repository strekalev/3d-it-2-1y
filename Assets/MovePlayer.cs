﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float speed = 1;
   
    void Update()
    {
        if (Input.GetAxis("Horizontal") != 0)
        {
            this.gameObject.transform.position
            += new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * speed,0,0);
        }
        if (Input.GetAxis("Vertical") != 0)
        {
            this.gameObject.transform.position
            += new Vector3(0, 0, Input.GetAxis("Vertical") * Time.deltaTime * speed);
        }
    }
}
